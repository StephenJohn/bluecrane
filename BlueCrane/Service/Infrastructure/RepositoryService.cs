﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BlueCrane.Infrastructure;
using System.Collections;

namespace BlueCrane.Service.Infrastructure
{
    public abstract class RepositoryService<T> where T : class
    {
        protected IRepository<T> GenericRepository;

        protected RepositoryService(IRepository<T> genericRepository)
        {
            GenericRepository = genericRepository;
        }

        protected RepositoryService()
        {

        }

        #region IRepositoryService<T> Members

        public void Commit()
        {
            GenericRepository.Commit();
        }

        public IEnumerable<T> GetAll()
        {
            return GenericRepository.GetAll();
        }

        public T GetById(long id)
        {
            return GenericRepository.GetById(id);
        }

        public IEnumerable<T> GetMany(Expression<Func<T, bool>> where)
        {
            return GenericRepository.GetMany(where);
        }

        public void Update(T entity)
        {


            GenericRepository.Update(entity);
            Save();
        }

        public void Insert(T entity)
        {
            GenericRepository.Add(entity);
            Save();
        }

        public void Update(IEnumerable<T> entities)
        {


            GenericRepository.Update(entities);
            Save();
        }

        public void Insert(IEnumerable<T> entities)
        {
            GenericRepository.Add(entities);
            Save();
        }

        public void Delete(long id)
        {
            var student = GenericRepository.GetById(id);
            GenericRepository.Delete(student);
            Save();
        }

        public void Delete(IEnumerable<T> entities)
        {
            GenericRepository.Delete(entities);
        }
        public void Save()
        {
            GenericRepository.Commit();
        }

        #endregion
    }
}