﻿using System;
using System.Collections.Generic;
using System.Linq;
using BlueCrane.Infrastructure;
using BlueCrane.Entity;
using BlueCrane.Repository;
using BlueCrane.Service.Infrastructure;
using System.Linq.Expressions;
namespace BlueCrane.Service
{

    public class CraneService : RepositoryService<Crane>, IRepositoryService<Crane>
    {
        public CraneService(ICraneRepository craneRepository)
            : base(craneRepository)
        {

        }

        public IEnumerable<Crane> Get(Expression<Func<Crane, bool>> filter = null, Func<IQueryable<Crane>, IOrderedQueryable<Crane>> orderBy = null, string includeProperties = "")
        {
            var applications = GenericRepository.Get(filter, orderBy, includeProperties);
            return applications;
        }
    }
}