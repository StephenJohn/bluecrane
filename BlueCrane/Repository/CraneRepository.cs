﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlueCrane.Infrastructure;
using BlueCrane.Entity;
namespace BlueCrane.Repository
{
    /// <summary>
    /// CraneRepository
    /// </summary>
    public class CraneRepository : RepositoryBase<Crane>, ICraneRepository
    {
        /// <summary>
        /// CraneRepository
        /// </summary>
        /// <param name="databaseFactory"></param>
        public CraneRepository(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
        }
    }

    /// <summary>
    /// ICraneRepository
    /// </summary>
    public interface ICraneRepository : IRepository<Crane>
    {

    }


}