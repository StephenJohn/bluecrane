﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BlueCrane.Service;
using BlueCrane.Service.Infrastructure;
using BlueCrane.Entity;
namespace BlueCrane.Controllers
{
    public class CraneController : ApiController
    {
        IRepositoryService<Crane> _craneService;

        public CraneController(IRepositoryService<Crane> craneService)
        {
            this._craneService = craneService;
        }


        [HttpGet]
        // GET api/<controller>
        public IEnumerable<Crane> GetAll()
        {
            return this._craneService.GetAll();
        }



        [HttpGet]
        // GET api/<controller>/5
        public Crane GetCrane(int id)
        {
            return this._craneService.GetById(id);
        }


        [HttpPost]
        // POST api/<controller>
        public IHttpActionResult Create([FromBody]Crane Crane)
        {
             this._craneService.Insert(Crane);
            return Ok();
        }


        [HttpPut]
        // PUT api/<controller>/5
        public IHttpActionResult Update([FromBody]Crane Crane)
        {
             this._craneService.Update(Crane);
            return Ok();
        }

        [HttpDelete]
        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            this._craneService.Delete(id);
        }
    }
}
