﻿namespace BlueCrane.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cranes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Capacity = c.Int(nullable: false),
                        Model = c.String(),
                        Manufacturer = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Cranes");
        }
    }
}
