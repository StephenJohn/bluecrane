﻿namespace BlueCrane.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddChasisNoField : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Cranes", newName: "Crane");
            AddColumn("dbo.Crane", "ChasisNo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Crane", "ChasisNo");
            RenameTable(name: "dbo.Crane", newName: "Cranes");
        }
    }
}
