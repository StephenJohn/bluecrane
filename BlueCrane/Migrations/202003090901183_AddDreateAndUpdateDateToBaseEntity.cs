﻿namespace BlueCrane.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDreateAndUpdateDateToBaseEntity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cranes", "CreatedAt", c => c.DateTime(nullable: false));
            AddColumn("dbo.Cranes", "UpdatedAt", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cranes", "UpdatedAt");
            DropColumn("dbo.Cranes", "CreatedAt");
        }
    }
}
