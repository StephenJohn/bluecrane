﻿using System.Data.Entity;
using BlueCrane.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
namespace BlueCrane.DB
{

    public class BlueCraneDBContext : DbContext
    {
        public BlueCraneDBContext() : base("BlueCrane")
        {

        }

        public BlueCraneDBContext(string nameOrConnectionString)
           : base(nameOrConnectionString)
        {

        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            Database.SetInitializer<BlueCraneDBContext>(null);
        }



        public DbSet<Crane> Crane { get; set; }

    }
}