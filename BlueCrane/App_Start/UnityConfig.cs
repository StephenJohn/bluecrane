using System.Web.Http;
using System.Data.Entity;
using Unity;
using Unity.WebApi;
using BlueCrane.Infrastructure;
using BlueCrane.Repository;
using BlueCrane.Service.Infrastructure;
using BlueCrane.Service;
using BlueCrane.Entity;
using BlueCrane.DB;
namespace BlueCrane
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService

            container.RegisterType<IDatabaseFactory, DatabaseFactory>();
            container.RegisterType<ICraneRepository, CraneRepository>();
            container.RegisterType<IRepositoryService<Crane>,CraneService>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}