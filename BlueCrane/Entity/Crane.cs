﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlueCrane.Entity
{
    public class Crane : BaseEntity<long>
    {
        public int Capacity { get; set; }
        public string Model { get; set; }

        public string Manufacturer { get; set; }

        public string ChasisNo { get; set; }
    }
}