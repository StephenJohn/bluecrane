﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace BlueCrane.Entity
{
    public class BaseEntity<T>
    {
        [Key]
        public T Id { get; set; }

        public DateTime CreatedAt { get; set; }
        /// <summary>
        /// Date last Updated. 
        /// </summary>    
        public DateTime UpdatedAt { get; set; }
    }
}