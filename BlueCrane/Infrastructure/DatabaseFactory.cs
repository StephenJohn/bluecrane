﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlueCrane.DB;
namespace BlueCrane.Infrastructure
{
   
    public class DatabaseFactory : Disposable, IDatabaseFactory
    {
        private BlueCraneDBContext _dataContext;

        public BlueCraneDBContext Get()
        {
            return _dataContext ?? (_dataContext = new BlueCraneDBContext());
        }

        protected override void DisposeCore(bool disose)
        {
            if (_dataContext != null)
                _dataContext.Dispose();
        }


    }
}