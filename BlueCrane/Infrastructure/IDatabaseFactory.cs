﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlueCrane.DB;
namespace BlueCrane.Infrastructure
{
   public interface IDatabaseFactory
    {
        BlueCraneDBContext Get();
    }
}
